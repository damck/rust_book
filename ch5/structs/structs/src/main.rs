#![allow(unused_variables)]
struct User {
    username: String,
    email: String,
    sign_in_count: u64,
    active: bool
}

struct Color(i32, i32, i32);
struct Point(i32, i32, i32);

fn main() {
    let user1 = build_user(String::from("aa@bb.com"), String::from("damcio"));

    println!("New user {} created!", user1.username);

    let user2 = User {
        email: String::from("bb@aa.com"),
        ..user1
    };

    println!("user {} email changed from {} to {}!", user2.username, user1.email, user2.email);

    let black = Color(0, 0, 0);
    let origin = Point(0, 0, 0);
}

fn build_user(email: String, username: String) -> User {
    User {
        email,
        username,
        active: true,
        sign_in_count: 0,
    }
}

