mod cels;
mod fibo;

use cels::*;

fn main() {
    let mut x = 5;
    println!("The value of x is {} and address of x is {:p}", x, &x);
    x = 6;
    println!("The value of x is {} and address of x is {:p}", x, &x);
    let y = 7;
    println!("The value of y is {} and address of y is {:p}", y, &y);
    let y: i64 = 8;
    println!("The value of y is {} and address of y is {:p}", y, &y);
    let binary: u8 = 0b0010_1000;
    println!("The value of binary is {} and address of binary is {:p}", binary, &binary);
    let character: char = 'ℤ';
    println!("The value of character is {} and address of character is {:p}", character, &character);
    let tuple1: (i32, f64, char) = (500, 6.4, 'F');
    println!("The value of tuple1 is {:?} and address of tuple1 is {:p}", tuple1, &tuple1);
    let tuple2: (i32, f64, u8) = (500, 6.4, 8);
    println!("The value of tuple2 is {:?} and address of tuple2 is {:p}", tuple2, &tuple2);

    let (_, b, _) = tuple2;
    println!("The value of b is {:?} and address of b is {:p}", b, &b);

    function_test(19);

    let z = {
        let temp = 1;
        temp + 1
    };
    println!("The value of z is {} and address of z is {:p}", z, &z);
    //println!("The value of temp is {} and address of temp is {:p}", temp, &temp);
    //not in scope anymore

    println!("The value of function is {} and address of function is {:p}", function_return(6), &function_return);

    let mut boolean: bool = true;

    while boolean {
        if boolean {
            println!("Yeah true");
            boolean = false;
        }
        else {
            println!("Nope");
        }
    }

    let expression = if boolean {
        "Was true"
    } else {
        "But wasn't"
    };
    println!("The value of expression is {} and address of expression is {:p}", expression, &expression);

    let mut number = 3;

    while number != 0 {
        println!("Countdown: {}", number);
        number = number - 1;
    }

    let arrayNum = [1, 2, 3, 4, 5];
    for el in arrayNum.iter() {
        println!("The element is: {}", el);
    }

    println!("Convert {} in celsius to fahrenheit: {}", 32., cels_to_fahr(32.));
    println!("Convert {} in fahrenheit to celsius: {}", 89.6, fahr_to_cels(89.6));

    println!("{}th fibonacci number is: {}", 10, fibo::fibo(10));
}

fn function_test(x: u32) {
    println!("The called number is: {}", x);
}

fn function_return(x: u32) -> u32 {
    x + x
}
