//fibonacci num generation

pub fn fibo(num: u32) -> u32 {
    if num < 3 {
        1
    } else {
        fibo(num - 1) + fibo(num - 2)
    }
}
