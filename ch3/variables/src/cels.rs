//Functions to convert between celsius and fahrenheit degree scales

pub fn cels_to_fahr(temp: f64) -> f64 {
    (temp * 9.0) / 5.0 + 32.0
}

pub fn fahr_to_cels(temp: f64) -> f64 {
    (temp - 32.) * 5. / 9.0
}
