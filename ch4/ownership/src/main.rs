fn main() {
    let s1 = String::from("Hello");
    let s2 = s1;

    println!("String: {}", s2);
    let mut s3 = append_and_return_world(s2);
    println!("String: {}", s3);

    append_excl_ref(&mut s3);
    println!("String: {}", s3); // didn't go out of scope

    append_excl(s3);
    //println!("String: {}", s3); // went out of scope
}

fn append_and_return_world(mut p_str : String) -> String {
    p_str.push_str(" world");
    p_str
}

fn append_excl(mut p_str : String) {
    p_str.push_str("!");
    println!("Appended: {}", p_str);
}

fn append_excl_ref(mut p_str : &mut String) {
    p_str.push_str("!");
}
