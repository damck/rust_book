fn main() {
    let str1 = String::from("Hello, world!");
    let end1 = first_word(&str1);
    println!("First word : {}", end1);

    let hello = &str1[0..5];
    let world = &str1[6..11];
}

fn first_word(s: &str) -> &str {
    let bytes = s.as_bytes();

    for (i, &item) in bytes.iter().enumerate() {
        if item == b' ' {
            return &s[0..i];
        }
    }

    &s[..]
}
