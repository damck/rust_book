#![allow(unused_variables)]

enum IpAddrKind {
    V4,
    V6
}

struct IpAddr {
    kind: IpAddrKind,
    address: String
}

enum IpAddrKind2 {
    V4(u8, u8, u8, u8),
    V6(String)
}

enum Message {
    Quit,
    Move(i32, i32),
    Write(String),
    ChangeColor(i32, i32, i32)
}

impl Message {
    fn call(&self) {
        //empty
    }
}

#[derive(Debug)]
enum UsState {
    Alabama,
    Alaska,
    Texas,
    Ohio
}

enum Coin {
    Penny,
    Nickel,
    Dime,
    Quarter(UsState)
}

fn main() {
    let home = IpAddr {
        kind: IpAddrKind::V4,
        address: String::from("127.0.0.1")
    };

    let loopback = IpAddr {
        kind: IpAddrKind::V6,
        address: String::from("::1")
    };

    let home_enum = IpAddrKind2::V4(127, 0, 0, 1);
    let loopback_enum = IpAddrKind2::V6(String::from("::1"));

    println!("The quarter is worth {} cents", value_in_cents(Coin::Quarter(UsState::Texas)));
    value_in_cents(Coin::Quarter(UsState::Texas));

    for num in 1..30 {
        fizzbuzz(num);
    }

    let coin = Coin::Quarter(UsState::Ohio);
    let mut count = 0;

    if let Coin::Quarter(state) = coin {
        println!("Quarter from state: {:?}", state);
    } else {
        count += 1;
    }
}

fn value_in_cents(coin: Coin) -> u32 {
    match coin {
        Coin::Penny => 1,
        Coin::Nickel => 5,
        Coin::Dime => 10,
        Coin::Quarter(state) => 25
    }
}

fn fizzbuzz(num: u32) {
    match (num%3, num%5, num%7) {
        (0, 0, 0) => println!("FizzBuzzBaz"),
        (0, 0, _) => println!("FizzBuzz"),
        (0, _, 0) => println!("FizzBaz"),
        (_, 0, 0) => println!("BuzzBaz"),
        (0, _, _) => println!("Fizz"),
        (_, 0, _) => println!("Buzz"),
        (_, _, 0) => println!("Baz"),
        (_, _, _) => println!("{}", num)
    }
}

